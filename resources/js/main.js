jQuery(document).ready(function($) {
	// Animations
	//-----------------------------------------------
	var delay=0, setTimeoutConst;
	if (($("[data-animation-effect]").length>0) && !Modernizr.touch) {
		$("[data-animation-effect]").each(function() {
			var item = $(this),
			animationEffect = item.attr("data-animation-effect");

			if(Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
				item.appear(function() {
					if(item.attr("data-effect-delay")) item.css("effect-delay", delay + "ms");
					setTimeout(function() {
						item.addClass('animated object-visible ' + animationEffect);

					}, item.attr("data-effect-delay"));
				}, {accX: 0, accY: -130});
			} else {
				item.addClass('object-visible');
			}
		});
	};

    // Table links
	//-----------------------------------------------
	$('table').on('click', 'tr[data-link]', function() {
		var link = $(this).attr('data-link');
		window.location = link;
	});

});

// Toggle PopOver
//-----------------------------------------------
function togglePopOver(id, toggle) {
	var target = $(id);
	var toggle = $(toggle)
	var active = $(".popOver.activePopOver:not('"+id+"')").length;

	if (active != 0) {
		$(".activePopOver").addClass("slideOutUp").removeClass('slideInDown activePopOver');
		$("[data-type='togglePop']").removeClass('active')
	}

	if (target.hasClass("activePopOver")) {
		target.addClass("slideOutUp").removeClass('slideInDown');
		setTimeout(function() {
			target.removeClass('activePopOver');
		}, 100)
	} else {
		target.addClass('slideInDown activePopOver').removeClass('slideOutUp');
	}
	toggle.toggleClass('active');
}


// Toggle Elements
//-----------------------------------------------
function toggleElement(element) {
	var targetElement = $("[data-toggle='target'][data-element='"+element+"']");
	var toggleElement = $("[data-toggle='button'][data-element='"+element+"']");
	if (targetElement.hasClass('active')) {
		if(targetElement.hasClass('ignore')) {

		} else {
			targetElement.removeClass('active');
			setTimeout(function() {
				targetElement.hide();
			}, 200);
			toggleElement.removeClass('active');
			if (element == "formsBar") {
				$(".form-container").removeClass('col-sm-8').addClass('col-sm-11');
			} else if (element == "camerasContainer") {
				$("#videoUI iframe").remove()
			}
		}
	} else {
		if (element == "formsBar") {
			$(".form-container").removeClass('col-sm-11').addClass('col-sm-8');
		}
		targetElement.addClass('active').removeClass('hidden').show();
		toggleElement.addClass('active');
	}
}

function hideToggleElement(element) {
	var targetElement = $("[data-toggle='target'][data-element='"+element+"'].active");
	var toggleElement = $("[data-toggle='button'][data-element='"+element+"'].active");
	targetElement.removeClass('active');
	setTimeout(function() {
		targetElement.hide();
	}, 200);
	toggleElement.removeClass('active');
	if (element == "formsBar") {
		$(".form-container").removeClass('col-sm-8').addClass('col-sm-11');
	}
}